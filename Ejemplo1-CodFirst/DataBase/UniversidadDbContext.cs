﻿using Ejemplo1_CodFirst.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejemplo1_CodFirst.DataBase
{
    public class UniversidadDbContext:DbContext
    {
        public UniversidadDbContext():base("Universidad")
        {
    
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Sede> Sedes { get; set; }
    }
}
