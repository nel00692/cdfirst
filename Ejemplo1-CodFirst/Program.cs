﻿using Ejemplo1_CodFirst.DataBase;
using Ejemplo1_CodFirst.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejemplo1_CodFirst
{
    class Program
    {
        static void Main(string[] args)
        {
            UniversidadDbContext _context;
            _context = new UniversidadDbContext();
            /*
            Sede sd = new Sede();
            sd.Nombre = "Santa Rosa de Copan";
            sd.Direccion = "Santa Rosa de Copan";
            sd.Estado = true;

            _context.Sedes.Add(sd);
            */
            var sed = _context.Sedes.Where(s => s.SedeId == 1).SingleOrDefault();
            sed.Nombre = "Copan";
            _context.SaveChanges();

            Console.WriteLine("Se actualizo el registro");
            Console.ReadKey();
        }
    }
}
