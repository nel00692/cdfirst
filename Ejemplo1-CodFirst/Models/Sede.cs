﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejemplo1_CodFirst.Models
{
    [Table("Sede")]
    public class Sede
    {
        [Key]
        public int SedeId { get; set; }
        
        [Required]
        [StringLength(50,ErrorMessage = "El nombre no puede ser mayor a 50 caracteres")]
        public string Nombre { get; set; }
        
        [MinLength(10,ErrorMessage ="La direccion debe ser mayor a 10 caracteres")]
        public string Direccion { get; set; }
        public bool Estado { get; set; }
    }
}
