﻿namespace Ejemplo1_CodFirst.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InicialTablaSede : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Sede",
                c => new
                    {
                        SedeId = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false, maxLength: 50),
                        Direccion = c.String(),
                        Estado = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.SedeId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Sede");
        }
    }
}
